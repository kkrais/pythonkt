#!/usr/bin/python
# -*- coding: utf-8 -*-
#Autor: Karl-Erik Krais
#Kirjeldus: skript, mis saab käsurealt 2 parameetrit: 1. sisendfaili nimi, 2. väljundfaili nimi
#Skript loeb sisendfailist Skriptimiskeelte ainesse registreerunud isikute nimed ja loob I-Teele sobiva väljundfaili (CSV). Faili laiend ei pea tingimata CSV olema.
#Numbri ja nime eraldaja on \t (tab), nime ja grupi eraldaja on tühik. Ideaalis peaks skript ignoreerima tühje ridu ja päist.
import random
import re
import sys
import os
 

 
filtered_lines = []
 
#Funktsioon, mis genereerib suvalise kombinatsiooni sümboleid
def generateToken(): 
    letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/_'
    token = ''
    for i in range(16):
        token += random.choice(letters)
    return token


def renderOutput():
    for line in filtered_lines:
        #Võtab eesnime esimese tähe
        firstLetter = line['firstname'][0] 
        #Võtab perekonnanime
        lastName = line['lastname']
        #Väärtustame kasutajanime, max8 tähte
        userName = (firstLetter + lastName)[0:8]
        #Väärtustame eesnime
        firstName = line['firstname']
        #Väärtustame grupi
        group = line['group']
        name = "%s %s" % (firstName, lastName)
        email = "%s.%s@itcollege.ee" % (firstName, lastName)
        token = "%s%s" % (group, generateToken())
        #Lõplik sõne, mis kirjutatakse faili
        finalString = "%s,%s,%s,%s" % (userName, name, email, token)
        file = open(outputfileUsr, "a")
        file.write(finalString + "\n")
 
#Funktsioon, mis eemaldab ära tühjad (\n) ja tabuleeritud (\t) read 
def filterLines(file):
    lines = file.readlines()
    while "\n" in lines: lines.remove("\n")
    #Eemaldab kõige ülemise rea
    lines.pop(0)
    #Itereerime kõik read läbi
    for idx, line in enumerate(lines):
        #Eemaldame newline-d
        line = re.sub(r'[\n]', "", line)
        #Eraldame igast reast tab-id
        line = line.split("\t")
        #Loome tühja dictionary
        data = {}
        #Paneme dictionary kohal id võrdeks stringiga kus eraldati esimene tab
        data['id'] = line[0]
        data['firstname'] = line[1].split(" ")[0].lower()
        data['lastname'] = line[1].split(" ")[1].lower()
        data['group'] = line[1].split(" ")[2].lower()
        #Lisame dictinary eraldatud väärtused
        filtered_lines.insert(idx, data)
 
#Salvestame kasutaja poolt sisestatud parameetrid muutujasse:
param = len(sys.argv)
#Parameetrite kontroll
if param != 3 :
    print "Vale kasutus"
else:
    #Salvestame sisendfaili ja väljundfaili muutujasse
    inputfileUsr = sys.argv[1]
    outputfileUsr = sys.argv[2]
    #Kas sisendfail eksisteerib:
    if os.path.exists(inputfileUsr):
        file = open(inputfileUsr, "r")
        filterLines(file) #Käivitame funktsiooni 
        file.close()
        renderOutput() #Käivitame funktsiooni
    else:
        print "Sisendfail ei eksisteeri"
        sys.exit()
    #Kas väljundfail eksisteerib, kui mitte siis loome:
    if os.path.exists(outputfileUsr) :
        file = open(outputfileUsr)
    else: 
        print "Väljundfail ei eksisteeri,loome uue faili"
        file = open(outputfileUsr, "a")
        file.close()




    






